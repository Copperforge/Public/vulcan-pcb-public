#!/bin/sh

kikit panelize \
--layout 'grid; rows: 3; cols: 3; vspace: 3mm; vbackbone: 10mm; hspace: 3mm' \
--tabs 'fixed; hwidth: 15mm; vcount: 0' \
--cuts vcuts \
--framing 'frame; width: 10mm; space: 3mm; cuts: none; slotwidth: 3mm' \
--post 'millradius: 1mm; ' \
--tooling '4hole; size: 2.05mm; hoffset: 2mm; voffset: 2mm' \
--fiducials '4fid; coppersize: 1.5mm; opening: 3mm; hoffset: 8mm; voffset: 8mm' \
--text 'simple; text: VulCAN Rev5 Panel, 27/01/2023, JLCJLCJLCJLC; anchor: mt; voffset: 2.5mm; hjustify: center; vjustify: center' \
vulcan_portal.kicad_pcb \
out/panel_out.kicad_pcb
#--copperfill 'hatched; clearance: 2mm; spacing: 0.5mm; width: 0.5mm' \
